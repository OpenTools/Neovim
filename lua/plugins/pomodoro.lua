return {
  "dbinagi/nomodoro",
  config = function()
    require("nomodoro").setup({
      work_time = 20,
      break_time = 5,
      menu_available = true,
      texts = {
        on_break_complete = "TIME IS UP!",
        on_work_complete = "TIME IS UP!",
        status_icon = "羽",
        timer_format = "!%0M:%0S", -- To include hours: '!%0H:%0M:%0S'
      },
      on_work_complete = function()
        os.execute("notify-send '🍅 Pomodoro Work Session Completed'")
      end,
      on_break_complete = function()
        os.execute("notify-send '🍅 Pomodoro Break is over'")
      end,
    })
  end,
  keys = {

    { "<leader>n", desc = "Nomodoro" },
    { "<leader>nw", "<cmd>NomoWork<cr>", desc = "Start Work" },
    { "<leader>nb", "<cmd>NomoBreak<cr>", desc = "Break" },
    { "<leader>ns", "<cmd>NomoStop<cr>", desc = "Stop" },
    { "<leader>nm", "<cmd>NomoStatus<cr>", desc = "Status" },
  },
}
