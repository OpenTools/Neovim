return {
  {
    "nvim-neotest/neotest",
    optional = true,
    dependencies = {
      "rouge8/neotest-rust",
      "nvim-neotest/neotest-python",
    },
    opts = {
      adapters = {
        ["neotest-rust"] = {},
        ["neotest-elixir"] = {},
        ["neotest-python"] = {
          -- Here you can specify the settings for the adapter, i.e.
          runner = "pytest",
          python = ".venv/bin/python",
        },
      },
    },
  },
  {
    "jfpedroza/neotest-elixir",
  },
  {
    "andythigpen/nvim-coverage",
    init = function()
      require("coverage").setup()
    end,
  },
}
