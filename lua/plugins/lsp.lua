return {
  {
    "simrat39/symbols-outline.nvim",
    cmd = "SymbolsOutline",
    keys = { { "<leader>cs", "<cmd>SymbolsOutline<cr>", desc = "Symbols Outline" } },
    config = true,
  },

  {
    "neovim/nvim-lspconfig",
    event = { "BufReadPre", "BufNewFile" },
    dependencies = {
      { "folke/neoconf.nvim", cmd = "Neoconf", config = false, dependencies = { "nvim-lspconfig" } },
      "mason.nvim",
      "williamboman/mason-lspconfig.nvim",
      -- {
      --   "hrsh7th/cmp-nvim-lsp",
      --   cond = function()
      --     return require("lazyvim.util").has("nvim-cmp")
      --   end,
      -- },
    },
    opts = {
      -- options for vim.diagnostic.config()
      diagnostics = {
        underline = true,
        update_in_insert = false,
        virtual_text = {
          spacing = 4,
          source = "if_many",
          prefix = "●",
          -- this will set set the prefix to a function that returns the diagnostics icon based on the severity
          -- this only works on a recent 0.10.0 build. Will be set to "●" when not supported
          -- prefix = "icons",
        },
        severity_sort = true,
      },
      -- Enable this to enable the builtin LSP inlay hints on Neovim >= 0.10.0
      -- Be aware that you also will need to properly configure your LSP server to
      -- provide the inlay hints.
      inlay_hints = {
        enabled = false,
      },
      -- add any global capabilities here
      capabilities = {},
      -- Enable this to show formatters used in a notification
      -- Useful for debugging formatter issues
      format_notify = false,
      -- options for vim.lsp.buf.format
      -- `bufnr` and `filter` is handled by the LazyVim formatter,
      -- but can be also overridden when specified
      format = {
        formatting_options = nil,
        timeout_ms = nil,
      },
      -- LSP Server Settings
      ---@type lspconfig.options
      servers = {
        --   texlab = {
        --     keys = {
        --       { "<Leader>K", "<plug>(vimtex-doc-package)", desc = "Vimtex Docs", silent = true },
        --     },
        --   },
        tinymist = {
          --- todo: these configuration from lspconfig maybe broken
          single_file_support = true,
          root_dir = function()
            return vim.fn.getcwd()
          end,
          --- See [Tinymist Server Configuration](https://github.com/Myriad-Dreamin/tinymist/blob/main/Configuration.md) for references.
          settings = {
            formatterMode = "typstyle",
            exportPdf = "onType",
            outputPath = "$root/target/$dir/$name",
          },
        },
        ltex = {
          --  filetypes = { "latex", "typst", "typ", "bib", "markdown", "plaintex", "tex", "org" },
          settings = {
            ltex = {
              --     enabled = { "latex", "typst", "typ", "bib", "markdown", "plaintex", "tex" },
              checkFrequency = "save",
              language = "en-US",
              sentenceCacheSize = 2000,
              dditionalRules = {
                enablePickyRules = true,
                motherTongue = "en-US",
                -- languageModel = '/Users/user/.local/apps/language_tool_ngram'
                --
              },
              dictionary = {
                ["en-US"] = {
                  "Abhoy",
                  "Aer",
                  "Arther",
                  "Bernstein-Vazirani",
                  "Borealis",
                  "Burgholzer",
                  "CNOT",
                  "Clauser",
                  "Coecke",
                  "DFKI",
                  "DQC",
                  "Deutsch",
                  "Deutsch-Jozsa",
                  "Euler decomposition",
                  "Hadamard",
                  "Hurwitz",
                  "Josza",
                  "Kamalika",
                  "Kole",
                  "MQT-QCEC",
                  "München",
                  "NISQ",
                  "Neovim",
                  "OpenQASM",
                  "POVM",
                  "Paremenides",
                  "PyCall",
                  "PyZX",
                  "PythonCall",
                  "QASM",
                  "QCEC",
                  "QMA",
                  "QMA-Complete",
                  "QPE",
                  "QPU",
                  "QPU",
                  "Qiskit",
                  "Quantinuum",
                  "Quantomatic",
                  "QuantumCircuitEquivalence",
                  "RIKEN",
                  "SQC",
                  "Shor",
                  "Technische",
                  "Toffoli",
                  "Universität",
                  "Wille",
                  "XNOR",
                  "ZX-Calclus",
                  "ZX-Calculus",
                  "ZX-Diagram",
                  "ZX-Diagrams",
                  "ZX-Graph",
                  "ZX-Graphs",
                  "Zeilinger",
                  "bra",
                  "bra-ket",
                  "bras",
                  "diagramic",
                  "e^i",
                  "eigenstates",
                  "involutory",
                  "jl",
                  "ket",
                  "kets",
                  "ltex-ls",
                  "mitter",
                  "multi-qubit",
                  "n-qubit",
                  "observables",
                  "picturalism",
                  "pre-socratic",
                  "qcecqmddalternative",
                  "qceczxm qcecconstruction",
                  "qubit",
                  "qubits",
                  "single-qubit",
                  "superpositions",
                  "transpiling",
                  "two-qubit",
                  "microfluidics",
                  "microchannels",
                  "electrowetting",
                },
              },
            },
          },
        },

        typst_lsp = {
          serverPath = "/usr/bin/typst-lsp",
          settings = {
            exportPdf = "onType", -- Choose onType, onSave or never.
            experimentalFormatterMode = "On",
          },
        },

        lua_ls = {
          -- mason = false, -- set to false if you don't want this server to be installed with mason
          -- Use this to add any additional keymaps
          -- for specific lsp servers,
          ---@type LazyKeys[]
          -- keys = {},
          settings = {
            Lua = {
              workspace = {
                checkThirdParty = false,
              },
              completion = {
                callSnippet = "Replace",
              },
            },
          },
        },
      },
      -- you can do any additional lsp server setup here
      -- return true if you don't want this server to be setup with lspconfig
      ---@type table<string, fun(server:string, opts:_.lspconfig.options):boolean?>
      setup = {
        -- example to setup with typescript.nvim
        -- tsserver = function(_, opts)
        --   require("typescript").setup({ server = opts })
        --   return true
        -- end,
        -- Specify * to use this function as a fallback for any server
        -- ["*"] = function(server, opts) end,
      },
    },
  },
  -- This is a VS Code like winbar that uses nvim-navic in order to get LSP context from your language server.
  {
    "utilyre/barbecue.nvim",
    name = "barbecue",
    version = "*",
    dependencies = {
      "SmiteshP/nvim-navic",
      "nvim-tree/nvim-web-devicons", -- optional dependency
    },
    opts = {
      -- configurations go here
      theme = "tokyonight",
    },
  },
  --  {
  --    "nvimtools/none-ls.nvim",
  --    event = { "BufReadPre", "BufNewFile" },
  --    dependencies = { "mason.nvim" },
  --    opts = function(_, opts)
  --      local nls = require("null-ls")
  --      return {
  --        --        root_dir = require("none-ls.utils").root_pattern(".none-ls-root", ".neoconf.json", "Makefile", ".git"),
  --        sources = {
  --          -- nls.builtins.formatting.fish_indent,
  --          -- nls.builtins.diagnostics.fish,
  --          nls.builtins.formatting.stylua,
  --          nls.builtins.formatting.shfmt,
  --          -- nls.builtins.diagnostics.flake8,
  --          nls.builtins.diagnostics.vale.with({
  --            extra_filetypes = { "org", "tex", "jl", "typ" },
  --          }),
  --        },
  --      }
  --    end,
  --  },

  {
    "lervag/vimtex",
    lazy = false, -- lazy-loading will disable inverse search
    config = function()
      vim.api.nvim_create_autocmd({ "FileType" }, {
        group = vim.api.nvim_create_augroup("lazyvim_vimtex_conceal", { clear = true }),
        pattern = { "bib", "tex" },
        callback = function()
          vim.wo.conceallevel = 2
        end,
      })

      vim.g.vimtex_mappings_disable = { ["n"] = { "K" } } -- disable `K` as it conflicts with LSP hover
      vim.g.vimtex_quickfix_method = vim.fn.executable("pplatex") == 1 and "pplatex" or "latexlog"
      vim.g.vimtex_view_general_viewer = "okular"
      vim.g.vimtex_view_general_options = "--unique file:@pdf#src:@line@tex"
    end,
  },
}
