return {
  { 
  "kaarmu/typst.vim",
  ft = "typ",
  lazy = false,
},
{
  'chomosuke/typst-preview.nvim',
  lazy = false, -- or ft = 'typst'
  version = '1.*',
  config = function()
    require 'typst-preview'.setup {}
  end,
},
}
