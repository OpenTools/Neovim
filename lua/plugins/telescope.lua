return {
  --   {
  --     "nvim-telescope/telescope.nvim",
  --     keys = {
  --       -- add a keymap to browse plugin files
  --       -- stylua: ignore
  --       {
  --         "<leader>fp",
  --         function() require("telescope.builtin").find_files({ cwd = require("lazy.core.config").options.root }) end,
  --         desc = "Find Plugin File",
  --       },
  --       {
  --         "<leader>fl",
  --         function()
  --           require("telescope-bibtex.actions").bibtex({ cwd = require("lazy.core.config").options.root })
  --         end,
  --         desc = "Find Bibtex Citations",
  --       },
  --     },
  --     -- change some options
  --     opts = {
  --       defaults = {
  --         layout_strategy = "horizontal",
  --         layout_config = { prompt_position = "top" },
  --         sorting_strategy = "ascending",
  --         winblend = 0,
  --       },
  --       extensions = {
  --         bibtex = {
  --           context = true,
  --           depth = 2,
  --           global_files = { vim.fn.expand("~/src/um-thesis/references.bib") },
  --         },
  --       },
  --     },
  --   },
  --
  --   -- add telescope-fzf-native
  --   {
  --     "telescope.nvim",
  --     dependencies = {
  --       "nvim-telescope/telescope-fzf-native.nvim",
  --       build = "make",
  --       config = function()
  --         require("telescope").load_extension("fzf")
  --       end,
  --     },
  --   },
  --   -- add telescope-bibtex
  --   {
  --     "telescope.nvim",
  --     dependencies = {
  --       "nvim-telescope/telescope-bibtex.nvim",
  --       config = function()
  --         require("telescope").load_extension("bibtex")
  --       end,
  --     },
  --   },
}
