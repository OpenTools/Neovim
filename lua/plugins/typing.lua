return {
  {
    "BYT0723/typist.nvim",
    opts = {
      {
        -- Space between two lines
        paddingLine = 1,
        -- neovim window config
        win = {
          relative = "win",
          title = "Typist",
          title_pos = "center",
          border = "double",
          style = "minimal",
          row = 0.5, -- float, meaning 50%, and so on
          col = 0.5,
          width = 0.4,
          height = 0.6,
        },
      },
    }, -- default config
  },
}
