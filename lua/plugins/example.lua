-- since this is just an example spec, don't actually load anything here and return an empty spec
-- stylua: ignore
if true then return {} end

-- every spec file under the "plugins" directory will be loaded automatically by lazy.nvim
--
-- In your plugin files, you can:
-- * add extra plugins
-- * disable/enabled LazyVim plugins
-- * override the configuration of LazyVim plugins
return {
  -- -- add gruvbox
  -- { "ellisonleao/gruvbox.nvim" },

  -- -- Configure LazyVim to load gruvbox
  -- {
  --   "LazyVim/LazyVim",
  --   opts = {
  --     colorscheme = "gruvbox",
  --   },
  -- },

  -- disable trouble
  { "folke/trouble.nvim", enabled = false },

  -- add symbols-outline
  {
    "simrat39/symbols-outline.nvim",
    cmd = "SymbolsOutline",
    keys = { { "<leader>cs", "<cmd>SymbolsOutline<cr>", desc = "Symbols Outline" } },
    config = true,
  },

  -- override nvim-cmp and add cmp-emoji
  {
    "hrsh7th/nvim-cmp",
    dependencies = { "hrsh7th/cmp-emoji" },
    ---@param opts cmp.ConfigSchema
    opts = function(_, opts)
      table.insert(opts.sources, { name = "emoji" })
    end,
  },

  -- change some telescope options and a keymap to browse plugin files

  -- add pyright to lspconfig
  -- {
  --   "neovim/nvim-lspconfig",
  --   ---@class PluginLspOpts
  --   opts = {
  --     ---@type lspconfig.options
  --     servers = {
  --       -- pyright will be automatically installed with mason and loaded with lspconfig
  --       pyright = {},
  --     },
  --   },
  -- },

  -- add tsserver and setup with typescript.nvim instead of lspconfig
  -- Fixme is this needed?
  -- TODO finish the development of this plugin
  --  {'nvim-treesitter/nvim-treesitter'},
  --    {
  --    dir = "~/src/OS/neovim/hfcc.nvim/",
  --    opts = {
  --      -- cf Setup
  --        api_token = "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", -- cf Install paragraph
  --    model = "bigcode/starcoder", -- can be a model ID or an http(s) endpoint
  --    -- parameters that are added to the request body
  --    query_params = {
  --      max_new_tokens = 60,
  --      temperature = 0.2,
  --      top_p = 0.95,
  --      stop_token = "<|endoftext|>",
  --    },
  --    -- set this if the model supports fill in the middle
  --    fim = {
  --      enabled = true,
  --      prefix = "<fim_prefix>",
  --      middle = "<fim_middle>",
  --      suffix = "<fim_suffix>",
  --    },
  --    debounce_ms = 8000,
  --    accept_keymap = "<Tab>",
  --    dismiss_keymap = "<S-Tab>",
  --
  --    }
  --  },
}
