return {
  -- Render Latex equations as rich text, when <leader>oP is pressed
  -- Sometime fails, when there are syntax errors.
  {
    "jbyuki/nabla.nvim",
    keys = {
      {
        "<leader>op",
        function()
          require("nabla").popup()
        end,
        desc = "Render Latex Popup",
      },
      {
        "<leader>oP",
        function()
          require("nabla").toggle_virt({ autogen = true })
        end,
        desc = "Toggle Latex Preview",
      },
    },
  },

  -- Setup org mode for neovim
  {
    "nvim-orgmode/orgmode",
    ft = { "org" },
    init = function()
      require("orgmode").setup({
        --org_agenda_files = { '~/org/*', '~/org/**/*' },
        -- Define files used for the agenda
        -- org_agenda_files = { "~/org/privat/**/*" },
        org_agenda_files = { "~/orga/*" },
        -- Define the default org file
        org_default_notes_file = "~/org/refile.org",
        -- Setup notifications, for scheduled appointments and deadlines
        --
        org_todo_keywords = { "TODO", "WAITING", "|", "DONE", "DELEGATED" },
        org_todo_keyword_faces = {
          WAITING = ":foreground blue :weight bold",
          DELEGATED = ":background #FFFFFF :slant italic :underline on",
          TODO = ":background #000000 :foreground red", -- overrides builtin color for `TODO` keyword
        },
        archive_location = "%s_archive::",
        -- check hightligh_latex_and_related

        org_custom_exports = {
          f = {
            label = "Export to RTF format",
            action = function(exporter)
              local current_file = vim.api.nvim_buf_get_name(0)
              local target = vim.fn.fnamemodify(current_file, ":p:r") .. ".rtf"
              local command = { "pandoc", current_file, "-o", target }
              local on_success = function(output)
                print("Success!")
                vim.api.nvim_echo({ { table.concat(output, "\n") } }, true, {})
              end
              local on_error = function(err)
                print("Error!")
                vim.api.nvim_echo({ { table.concat(err, "\n"), "ErrorMsg" } }, true, {})
              end
              return exporter(command, target, on_success, on_error)
            end,
          },
          t = {
            label = "Export to Typst",
            action = function(exporter)
              local current_file = vim.api.nvim_buf_get_name(0)
              local target = vim.fn.fnamemodify(current_file, ":p:r") .. ".pdf"
              local command = { "pandoc", current_file, "--pdf-engine", "typst", "-o", target } -- a space is inserted by a new table item
              local on_success = function(output)
                print("Successfull conversion into Typst")
                vim.api.nvim_echo({ { table.concat(output, "\n") } }, true, {})
                -- TODO open in pdf viewer, if not open already
              end
              local on_error = function(err)
                print("Error!")
                vim.api.nvim_echo({ { table.concat(err, "\n"), "ErrorMsg" } }, true, {})
              end

              return exporter(command, target, on_success, on_error)
            end,
          },
        },
        capture_templates = {
          e = {
            description = "Event",
            subtemplates = {
              r = {
                description = "recurring",
                template = "** %?\n %T",
                target = "~/org/calendar.org",
                headline = "recurring",
              },
              o = {
                description = "one-time",
                template = "** %?\n %T",
                target = "~/org/calendar.org",
                headline = "one-time",
              },
            },
          },
          J = {
            description = "Journal",
            template = "\n*** %<%Y-%m-%d> %<%A>\n**** %U\n\n%?",
            target = "~/org/journal/%<%Y-%m>.org",
          },
          t = {
            description = "Task",
            template = "* TODO %?\n  %u",
          },
        },
        notifications = {
          enabled = true,
          cron_enabled = true,
          repeater_reminder_time = { 1, 5, 10 },
          deadline_warning_reminder_time = { 1, 5, 10 },
          reminder_time = 10,
          deadline_reminder = true,
          scheduled_reminder = true,
          notifier = function(tasks)
            local result = {}
            for _, task in ipairs(tasks) do
              require("orgmode.utils").concat(result, {
                string.format("# %s (%s)", task.category, task.humanized_duration),
                string.format("%s %s %s", string.rep("*", task.level), task.todo, task.title),
                string.format("%s: <%s>", task.type, task.time:to_string()),
              })
            end

            if not vim.tbl_isempty(result) then
              require("orgmode.notifications.notification_popup"):new({ content = result })
            end
          end,
          -- Create a cron notifier, which tries to send reminders to the DE notifications
          cron_notifier = function(tasks)
            for _, task in ipairs(tasks) do
              local title = string.format("%s (%s)", task.category, task.humanized_duration)
              local subtitle = string.format("%s %s %s", string.rep("*", task.level), task.todo, task.title)
              local date = string.format("%s: %s", task.type, task.time:to_string())

              -- Linux
              if vim.fn.executable("notify-send") == 1 then
                vim.loop.spawn("notify-send", { args = { string.format("%s\n%s\n%s", title, subtitle, date) } })
              end
            end
          end,
        },
      })
    end,
  },

  -- Add tablle mode
  { "dhruvasagar/vim-table-mode" },

  -- Add telescope for org
  { "joaomsa/telescope-orgmode.nvim" },
  {
    "lukas-reineke/headlines.nvim",
    dependencies = "nvim-treesitter/nvim-treesitter",
    config = true, -- or `opts = {}`
  },
}
