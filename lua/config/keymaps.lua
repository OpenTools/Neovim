-- Keymaps are automatically loaded on the VeryLazy event
-- Default keymaps that are always set: https://github.comLazyVim/LazyVim/blob/main/lua/lazyvim/config/keymaps.lua
-- Add any additional keymaps here

-- Open a PDF in Zathura
local map = vim.keymap.set
map("n", "<leader>of", ':silent exec "!zathura --fork " .. expand("%:p:r") .. ".pdf &"<CR>', { desc = "Open as PDF" })
